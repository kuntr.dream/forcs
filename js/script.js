$('.lc_sidebar_item_maintitle').on('click', function () {
   $(this).toggleClass('active')
   $(this).next().slideToggle(500)
})

$('.tab_item').on('click', function () {
   $('.tab_item').removeClass('active')
   $(this).addClass('active')
})

$('.first_item_tab').on('click', function () {
   $('.tab_list_item').removeClass('active')
   $('.first_tab').addClass('active')
})

$('.second_item_tab').on('click', function () {
   $('.tab_list_item').removeClass('active')
   $('.second_tab').addClass('active')
})

$('.icon_password').on('click', function () {
   $(this).toggleClass('active')
   $(this).parents('.input_item').find('.icon_password_watch').toggleClass('active')
   if ($(this).hasClass('active')) {
      $(this).parents('.input_item').find('input').attr('type', 'text')
   } else {
      $(this).parents('.input_item').find('input').attr('type', 'password')
   }
})

$('.vin_search_result_item_head').on('click', function () {
   $(this).toggleClass('active')
   $(this).next().slideToggle(500)
})

$('.api_item').on('click', function () {
   $(this).toggleClass('active')
   $(this).find('.api_content').slideToggle(500)
})

$('.catalog_sidebar_item_title').on('click', function () {
   $(this).toggleClass('active')
   $(this).next().slideToggle(400)
})

$('.catalog_body_head_filter_item_title').on('click', function () {
   $(this).next().addClass('active')
})

$('.catalog_body_head_filter_item_list_item').on('click', function () {
   $(this).parents('.catalog_body_head_filter_item').find('.catalog_body_head_filter_item_title span').text($(this).find('.title').text())
   $(this).parents('.catalog_body_head_filter_item').find('.catalog_body_head_filter_item_list').removeClass('active')
   $('.catalog_body_head_filter_item_list_item').removeClass('active')
   $(this).addClass('active')
   return false;
})

$(document).mouseup(function (e) {
   var container = $(".catalog_body_head_filter_item_list");
   if (container.has(e.target).length === 0) {
      container.removeClass('active');
   }
});

$('.catalog_body_content .btn_blue').on('click', function () {
   $(this).css('display', 'none')
   $(this).next().css('display', 'flex')
   return false;
})

$('.card_tab_head_item').on('click', function () {
   $('.card_tab_head_item, .accord_item').removeClass('active')
   $(this).addClass('active')
   return false;
})

$('.how_buy_wrppaer_body_tab_item').on('click', function () {
   $('.how_buy_wrppaer_body_tab_item, .how_buy_wrppaer_body_tab_list_block, .my_tab_item').removeClass('active')
   $(this).addClass('active')
   return false;
})

$('.info_modal_wrapper_right_tab_list_item').on('click', function () {
   $('.info_modal_wrapper_right_tab_list_item, .my_tab_item').removeClass('active')
   return false;
})

$(".open-tab").on("click", function () {
   $('.tab[data-tab="' + $(this).data('tab') + '"]').addClass("active");
});

$(".open-modal").on("click", function () {
   $('[data-modal="' + $(this).data('modal') + '"]').toggleClass("active");
   $('html').toggleClass('lock')
   return false;
});

$(".close-modal").on("click", function () {
   $('[data-modal="' + $(this).data('modal') + '"]').removeClass("active");
   $('html').removeClass('lock')
   return false;
});

$('.how_buy_wrppaer_body_tab_list_item_slide_title').on('click', function () {
   $(this).toggleClass('active')
   $(this).next().slideToggle(500)
})

$(document).on('click', function (e) {
   var target = $(e.target);

   if ($(target).hasClass("active")) {
      $(target).each(function () {
         $(this).removeClass("active");
         console.log('text')
      });
      console.log('text')
      setTimeout(function () {
         $(target).removeClass("active");
      }, 350);
      console.log('text')
   }

});

$('.sidebar_map_item').on('click', function () {
   $('.sidebar_map_item').removeClass('active')
   $(this).addClass('active')
})

$('.order_tab_item').on('click', function () {
   $('.order_tab_item').removeClass('active')
   $(this).addClass('active')
   return false;
})

$('.cart_head_filter_item_title').on('click', function () {
   $(this).next().addClass('active')
})

$('.cart_head_filter_item_list_item').on('click', function () {
   $(this).parents('.cart_head_filter').find('.cart_head_filter_busket span').text($(this).text())
   $(this).parents('.cart_head_filter').find('.cart_head_filter_item_list').removeClass('active')
   return false;
})

$('.cart_head_filter_item .close_icon').on('click', function () {
   $('.cart_head_filter_item').removeClass('active')
})

$(document).mouseup(function (e) {
   var container = $(".cart_head_filter_item_list");
   if (container.has(e.target).length === 0) {
      container.removeClass('active');
   }
});

$('.lc_body_list_item_title_mob').on('click', function () {
   $(this).toggleClass('active')
   $(this).next().slideToggle(500)
})

$(document).on('click', function (e) {
   var container = $(".popup");
   if (container.has(e.target).length === 0) {
      $('html').removeClass('lock');
      $('.fly_btn_item').removeClass('active');
   }
});


$('.select').on('click', function () {
   $(this).toggleClass('active')
})

$('.select_list_item').on('click', function () {
   $(this).parents('.select').find('.select_title span').text($(this).text())
   $('.select').removeClass('active')
   return false;
})

$('.select_check').on('click', function () {
   $(this).toggleClass('active')
})

$('.select_list_item_check').on('click', function () {
   $('.select').removeClass('active')
})

$(document).mouseup(function (e) {
   var container = $(".select");
   if (container.has(e.target).length === 0) {
      container.removeClass('active');
   }
});

$(document).mouseup(function (e) {
   var container = $(".select_check");
   if (container.has(e.target).length === 0) {
      container.removeClass('active');
   }
});

$('.btn_show_all, .show_all').on('click', function () {
   $('.category_sidebar_item_list').css('display', 'block')
})

$('.mobile_accord_title').on('click', function () {
   $(this).toggleClass('active')
   $(this).next().slideToggle({
      duration: 500,
      easing: "linear",
   });
})

if ($(window).width() < 767) {
   $('.catalog_body_head_filter_item_title .sort').text('Сортировка')
}
if ($(window).width() < 1200 && $(window).width() > 767) {
   $('.cart_block_list_item .icon').on('click', function () {
      $(this).parents('.cart_block_list_item').addClass('active')
   })
   $('.cart_block_list_item .icon_close_card').on('click', function () {
      $(this).parents('.cart_block_list_item').removeClass('active')
   })
}

$('.swiper_city_list li').on('click', function () {
   $('.swiper_city_list li').removeClass('active')
   $(this).addClass('active')
   $('.city_select').removeClass('active')
   $('body, html').removeClass('lock')
   $('.header_city span').text($(this).text())
   return false;
})

$('.cart_head_filter_item_list_item').on('click', function () {
   $('.cart_head_filter_item ').removeClass('active')
   $('.cart_head_filter_item_title span').text($(this).find('.cart_head_filter_item_list_item_title').text())
})

$('.action_btn').on('click', function () {
   $(this).addClass('active')
})

$('.cart_action_popup_wrapper_title .close_icon').on('click', function () {
   $('.cart_action_popup').removeClass('active')
   $('.action_btn').removeClass('active')
})

$('.cart_action_popup_wrapper_list_item').on('click', function () {
   $('.cart_action_popup ').removeClass('active')
   $('.action_btn').removeClass('active')
})


$('.basket_counter .plus').on('click', function () {
   count = $(this).parents('.basket_counter').find('input').val()
   if (count > 0) {
      $(this).parents('.basket_counter').find('input').val(++count)
   }
   if (count < 0) {
      count = 1
      $(this).parents('.basket_counter').find('input').val(count)
   }
})

$('.basket_counter .minus').on('click', function () {
   count = $(this).parents('.basket_counter').find('input').val()
   if (count > 1) {
      $(this).parents('.basket_counter').find('input').val(--count)
   }
   if (count < 0) {
      count = 1
      $(this).parents('.basket_counter').find('input').val(count)
   }
})

$(document).mouseup(function (e) {
   var container = $(".city_select");
   if (container.has(e.target).length === 0) {
      container.removeClass('active');
   }
});

$('.info_back').on('click', function () {
   $('.info_tab_content_item').removeClass('active')
   $('.info_tab_content').removeClass('active')
   $('.info_modal_wrapper_right_tab_list').addClass('active')
   $('.info_modal_wrapper_left').removeClass('active')
})

$('.info_modal_wrapper_right_tab_list_item').on('click', function () {
   $('.info_modal_wrapper_right_tab_list').removeClass('active')
   $('.info_modal_wrapper_left').addClass('active')
})

$('.search_buy .btn_blue').on('click', function () {
   $(this).css('display', 'none')
   $(this).next().css('display', 'flex')
   return false
})


var $range = $(".range");
var $inputFrom = $(".input_from");
var $inputTo = $(".input_to");
var instance;
var min = 0;
var max = 1000;
var from = 0;
var to = 0;

$range.ionRangeSlider({
   skin: "round",
   type: "double",
   min: min,
   max: max,
   onStart: updateInputs,
   onChange: updateInputs,
   onFinish: updateInputs
});
instance = $range.data("ionRangeSlider");

function updateInputs(data) {
   from = data.from;
   to = data.to;

   $inputFrom.prop("value", from);
   $inputTo.prop("value", to);
}

$inputFrom.on("change", function () {
   var val = $(this).prop("value");

   // validate
   if (val < min) {
      val = min;
   } else if (val > to) {
      val = to;
   }

   instance.update({
      from: val
   });

   $(this).prop("value", val);

});

$inputTo.on("change", function () {
   var val = $(this).prop("value");

   // validate
   if (val < from) {
      val = from;
   } else if (val > max) {
      val = max;
   }

   instance.update({
      to: val
   });

   $(this).prop("value", val);
});


const swiper_thumb_image = new Swiper('.swiper_thumb_image', {
   loop: false,
   breakpoints: {
      // when window width is >= 320px
      320: {
         slidesPerView: 2,
         spaceBetween: 20
      },
      // when window width is >= 480px
      767: {
         slidesPerView: 3,
         spaceBetween: 10,
         direction: 'vertical',
         slidesPerView: 'auto',
      },
      // when window width is >= 640px
      1201: {
         slidesPerView: 4,
         spaceBetween: 13,
         direction: 'horizontal',
         slidesPerView: 'auto',
      }
   }
});

const swiper_big_image = new Swiper('.swiper_big_image', {
   // Optional parameters
   direction: 'horizontal',
   loop: false,
   slidesPerView: 1,
   spaceBetween: 20,
   pagination: {
      el: '.swiper-pagination',
   },
   thumbs: {
      swiper: swiper_thumb_image,
   },
});


const index_swiper = new Swiper('.index_swiper', {
   // Optional parameters
   direction: 'horizontal',
   loop: false,
   pagination: {
      el: '.swiper-pagination',
   },
   slidesPerView: 1,
   spaceBetween: 0,
   navigation: {
      nextEl: '.swiper_index_btn_next',
      prevEl: '.swiper_index_btn_prev',
   },
   breakpoints: {
      // when window width is >= 320px
      320: {
         slidesPerView: 1,
         spaceBetween: 20
      },
      1201: {
         slidesPerView: 1,
         direction: 'horizontal',
      }
   }
});

const index_swiper_day = new Swiper('.index_swiper_day', {
   // Optional parameters
   direction: 'horizontal',
   loop: false,
   pagination: {
      el: '.swiper-pagination',
   },
   navigation: {
      nextEl: '.swiper_index_day_btn_next',
      prevEl: '.swiper_index_day_btn_prev',
   },
   breakpoints: {
      // when window width is >= 320px
      320: {
         slidesPerView: 1,
         spaceBetween: 20
      },
      767: {
         slidesPerView: 'auto',
         spaceBetween: 20
      },
      1440: {
         slidesPerView: 2,
         direction: 'horizontal',
         spaceBetween: 25,
      }
   }
});

const swiper_analog = new Swiper('.swiper_analog', {
   // Optional parameters
   direction: 'horizontal',
   loop: false,
   slidesPerView: 3,
   spaceBetween: 20,

   navigation: {
      nextEl: '.swiper_btn_analog_next',
      prevEl: '.swiper_btn_analog_prev',
   },
});

const swiper_city = new Swiper('.swiper_city', {
   // Optional parameters
   direction: 'horizontal',
   loop: false,
   slidesPerView: 1,
   spaceBetween: 20,
   effect: 'fade',
   fadeEffect: {
      crossFade: true
   },

   navigation: {
      nextEl: '.swiper_btn_city_next',
      prevEl: '.swiper_btn_city_prev',
   },
});

$('.all_check').on('change', function () {
   if ($(this).is(':checked')) {
      $('.cart_block_list .checkbox_item .custom-checkbox').prop('checked', true);
   } else {
      $('.cart_block_list .checkbox_item .custom-checkbox').prop('checked', false);
   }
})


$('body').on('click', '.action_btn.duplicate', function () {
   $('.actionbutton').addClass('active');
   product = $(this).parent().parent().prop('outerHTML');
   action_cart = 'duplicate';
   return false;
});


$('body').on('click', '.card_block_action_btn.duplicate, .cart_action_popup_wrapper_list_item.duplicate', function () {

   if ($('.cart_block_list_item .custom-checkbox').is(':checked')) {
      product = '';
      $('.cart_block_list_item .custom-checkbox:checked').each(function () {
         product = product + $(this).parent().parent().prop('outerHTML');
         action_cart = 'duplicate';
         $('.actionbutton').addClass('active');

      })
   } else {
      alert('Вы ничего не выбрали');
   }
})


$('body').on('click', '.card_block_action_btn.move, .cart_action_popup_wrapper_list_item.move', function () {

   if ($('.cart_block_list_item .custom-checkbox').is(':checked')) {
      product = '';
      $('.cart_block_list_item .custom-checkbox:checked').each(function () {
         product = product + $(this).parent().parent().prop('outerHTML');
         $(this).parent().parent().remove();
         action_cart = 'move';
         $('.actionbutton').addClass('active');
      })
   } else {
      alert('Вы ничего не выбрали');
   }
})



$('body').on('click', '.action_btn.move', function () {
   $('.actionbutton').addClass('active');
   product = $(this).parent().parent().prop('outerHTML');
   $(this).parent().parent().remove();
   action_cart = 'move';
   return false;
});

$('body').on('click', '.card_block_action_btn.remove, .cart_action_popup_wrapper_list_item.remove', function () {

   if ($('.cart_block_list_item .custom-checkbox').is(':checked')) {
      product = '';
      $('.cart_block_list_item .custom-checkbox:checked').each(function () {
         product = product + $(this).parent().parent().prop('outerHTML');
         $(this).parent().parent().remove();
      })
   } else {
      alert('Вы ничего не выбрали');
   }
})

$('.btn_search ').on('click', function () {
   $('.modal_search .header_search input').focus()
})


//Фильтр
$('.select_list_item_check input').on('change', function () {
   if ($(this).val() == 'all') {
      $('.select_title_check span').text($(this).next().text());
      $('.select_list_item_check input:not([value="all"])').prop('checked', false);
   } else {
      $('.select_list_item_check input[value="all"]').prop('checked', false);
      let checkboxes = $('.select_list_item_check input:checked').map(function () {
         return $(this).next().text();
      }).get().join(', ');
      $('.select_title_check span').text(checkboxes);
   }
})
//Фильтр

function titlePos() {
   let arr = $('.api_title_body').each(function () {
      let attr = $(this).data('title')
      $(this).prepend(function (atrr) {
         return $('<span>', {
            text: (attr)
         })
      })
   })
}

// Каталог
function titlePosCatalog() {
   if ($(window).width() < 767) {
      let arr = $('.data_pos').each(function () {
         let attr = $(this).data('title')
         $(this).prepend(function (atrr) {
            return $('<span>', {
               text: (attr)
            })
         })
      })
      $('.data_pos').children('span').remove()
   } else if ($(window).width() > 768) {
      $('.data_pos span').remove()
   }

}
// Карточка товара
function cardPosBlock() {
   $('.card_buy, .card_warning').appendTo('.card_buy_pos_mobile')
   $('.btn_back.mobile').appendTo('.pos_mobile')
   $('.login_warning_text p').appendTo('.form_login_text')
   $('.cart_head_filter_item_title').appendTo('.cart_head_filter_busket')
   $('.cart_head_filter_item_title span').text('Основная')
}

function titleRelace() {
   $('.cart_block_head .title').text('Выбрать все')
}

function card_pos() {
   $('.card_pos').appendTo('.card_pos_mobile')
}

if ($(window).width() < 1200) {
   card_pos()
}

if ($(window).width() < 767) {
   // 
   titlePos()
   // Каталог
   titlePosCatalog()
   // Карточка товара
   cardPosBlock()
   // 
   titleRelace()
}

// $(window).on('resize', function () {
//    cardPosBlock()
// });

$(".open-cart").on("click", function () {
   $('.cart_block_list').fadeOut(0)
   $('.cart_tab[data-cart-title="' + $(this).data('cart-title') + '"]').fadeIn(800);
   if (typeof action_cart !== 'undefined' && action_cart) {
      if (action_cart == 'duplicate') {
         $('.cart_tab[data-cart-title="' + $(this).data('cart-title') + '"]').append(product);
         $('.popup.actionbutton').removeClass('active');
         action_cart = undefined;
      } else {
         $('.cart_tab[data-cart-title="' + $(this).data('cart-title') + '"]').append(product);
         $('.popup.actionbutton').removeClass('active');
         action_cart = undefined;
      }
   }
   return false;
});


if ($(window).width() < 767) {
   $('.cart_head_filter_item_title').on('click', function () {
      $('.cart_head_filter_item_list').toggleClass('active')
      $('.cart_head_filter_item').addClass('active')
   })

   $('.cart_head_filter_item_list_item').on('click', function () {
      $('.cart_head_filter_item_title span').text($(this).text())
   })

   $(document).mouseup(function (e) {
      var container = $(".cart_head_filter_item");
      if (container.has(e.target).length === 0) {
         container.removeClass('active');
      }
   });
}

$('.cart_head_filter_item_list_item').on('click', function () {
   $('.cart_head_filter_item_title span').text($(this).text())
})

// $('.cart_block .checkbox_item').on('click', function () {
//    $(this).find('input').toggleClass('check')
//    if ($(this).find('input').hasClass('check')) {
//       $(this).find('input').attr('checked', 'checked')
//    } else {
//       $(this).find('input').removeAttr('checked', 'checked')
//       $(this).find('input').removeClass('check')
//    }
// })

// $('.cart_block .checkbox_item.all_check').on('click', function () {
//    $(this).toggleClass('active')
//    if ($(this).hasClass('active')) {
//       $('.cart_block .checkbox_item input').attr('checked', 'checked')
//    } else {
//       $('.cart_block .checkbox_item input').removeAttr('checked', 'checked')
//    }
// })

$('.cart_block_list_item .action .action_btn.remove').on('click', function () {
   $(this).parents('.cart_block_list_item').remove()
})

function buttonI() {
   if ($(window).width() < 767) {
      $('.info_tab_content_item').removeClass('active')
      $('.info_modal_wrapper_right_tab_list').addClass('active')
      $('.info_modal_wrapper_left').removeClass('active')
   } else {
      $('.info_tab_content_item').first().addClass('active')
      $('.info_modal_wrapper_right_tab_list').removeClass('active')
      $('.info_modal_wrapper_left').addClass('active')
   }
}

buttonI()

function cardResize() {
   if ($(window).width() < 767) {
      $('.mobile_accord_title').removeClass('active')
   } else {
      $('.card_tab_body_item').css('display', 'block')
   }
}

buttonI()
cardResize()

$(window).on('resize', function () {
   buttonI()
   cardResize()
});

$('.order_tab_item').on('click change', function () {
   $('.input_item input').each(function () {
      if ($(this).val() == "" && $(this).prop('required')) {
         $('.order_tab_block').children().last().removeClass('active')
         $('.order_tab_block').children().first().addClass('active')
         $(this).focus()
      } else {
         $('.order_tab_block').children().last().addClass('active')
         $('.order_tab_block').children().first().removeClass('active')
      }
   })
   $('.input_item .select_title span').each(function () {
      if ($(this).text() === 'Выберите из списка') {
         $('.order_tab_block').children().last().removeClass('active')
         $('.order_tab_block').children().first().addClass('active')
         $(this).parents('.input_item .select_title').addClass('bad')
         $(this).focus()
      } else {
         $(this).parents('.input_item .select_title').removeClass('bad')
      }
   })
})

$('.actionbutton .cart_head_filter_item_list_item ').on('click', function () {
   $('.cart_head_filter_busket span').text($(this).text())
})

$('.catalog_body_head_filter_item_list_head .close').on('click', function () {
   $('.catalog_body_head_filter_item_list').removeClass('active')
})

function slideNum() {
   if ($('.index_swiper .swiper-wrapper .swiper-slide').length < 10) {
      $('.index_swiper .total_slide').text('0' + $('.index_swiper .swiper-wrapper .swiper-slide').length)
   } else {
      $('.index_swiper .total_slide').text($('.index_swiper .swiper-wrapper .swiper-slide').length)
   }
   if ($('.index_swiper_day .swiper-wrapper .swiper-slide').length < 10) {
      $('.index_swiper_day .total_slide').text('0' + $('.index_swiper_day .swiper-wrapper .swiper-slide').length)
   } else {
      $('.index_swiper_day .total_slide').text($('.index_swiper_day .swiper-wrapper .swiper-slide').length)
   }
}
slideNum()

index_swiper.on('slideChange', function () {
   let currentSlide = index_swiper.activeIndex + 1
   if (currentSlide < 10) {
      $('.index_swiper .current_slide').text('0' + currentSlide)
   } else {
      $('.index_swiper .current_slide').text(currentSlide)
   }
})

$('.index_swiper_day .swiper-slide').each(function (index) {
   index = index + 1
   if (index < 10) {
      $(this).find('.current_slide').text('0' + index)
   } else {
      $(this).find('.current_slide').text(index)
   }

})

const swiper_how = new Swiper('.swiper_how', {
   loop: false,
   direction: 'horizontal',
   freeMode: {
      enabled: true,
   },
   watchSlidesProgress: true,
   simulateTouch: false,
   breakpoints: {
      1: {
         slidesPerView: 3,
      },
      1100: {
         slidesPerView: 4,
      }
   }
});

//Перепись под карты начало
if ($(window).width() < 1200) {
   ymaps.ready(inits);
   function inits() {
      let data_init = $('.sidebar_map_item_block:first-child .sidebar_map_item '),
         long = data_init.data('long'),
         lat = data_init.data('lat'),
         it = data_init.parent().find('.map_mobile').attr('id');
      $('#' + it).slideDown(500, function () {
         map = new ymaps.Map(it, {
            center: [lat, long],
            zoom: 15,
         });
         pin = new ymaps.Placemark([lat, long], {}, { preset: 'islands#redIcon' });
         map.geoObjects.add(pin);

      });
   }

   $('.sidebar_map_item').on('click', function () {
      $(this).addClass('active');
      let long = $(this).data('long'),
         lat = $(this).data('lat'),
         it = $(this).next().attr('id');
      $('.map_mobile').slideUp();
      $(this).next().slideToggle(500, function () {
         map.destroy();
         map = new ymaps.Map(it, {
            center: [lat, long],
            zoom: 15,
         });
         pin = new ymaps.Placemark([lat, long], {}, { preset: 'islands#redIcon' });
         map.geoObjects.add(pin);
      });

   })
}
//Перепись под карты конец

//Перепись под карты начало
$('.sidebar_map_item').on('click', function () {
   $('.sidebar_map_item').removeClass('active')
   $(this).addClass('active')
   let long = $(this).data('long'),
      lat = $(this).data('lat');
   map.setCenter([lat, long]);
})
//Перепись под карты конец






